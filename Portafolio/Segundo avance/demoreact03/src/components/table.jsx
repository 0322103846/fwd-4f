import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

const columns = [
  {
    name: 'ID',
    selector: (row) => row.id,
    sortable: true,
  },
  {
    name: 'Title',
    selector: (row) => row.title,
    sortable: true,
  },
  {
    name: 'Description',
    selector: (row) => row.description,
    sortable: true,
  },
  {
    name: 'Price',
    selector: (row) => `$${row.price}`,
    sortable: true,
    right: true,
  },
];

const FakeStoreApiDataTable = () => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch('https://fakestoreapi.com/products');
      const json = await response.json();
      setData(json);
    };

    fetchData();
  }, []);

  return (
    <DataTable
      columns={columns}
      data={data}
    />
  );
};

export default FakeStoreApiDataTable;