

export default function Content(){

    return(
        <div class="container px-4 py-5" id="hanging-icons">
            <h2 class="pb-2 border-bottom"></h2>
            <div class="row g-4 py-5 row-cols-1 row-cols-lg-3">
                <div class="col d-flex align-items-start">
                    <div class="icon-square text-body-emphasis bg-body-secondary d-inline-flex align-items-center justify-content-center fs-4 flex-shrink-0 me-3">
                    </div>
                    <div>
                    <h3 class="fs-2 text-body-emphasis">Titulo 1</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="#" class="btn btn-primary">
                        Primary button
                    </a>
                    </div>
                </div>
                <div class="col d-flex align-items-start">
                    <div class="icon-square text-body-emphasis bg-body-secondary d-inline-flex align-items-center justify-content-center fs-4 flex-shrink-0 me-3">
                    </div>
                    <div>
                    <h3 class="fs-2 text-body-emphasis">Titulo 2</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="#" class="btn btn-primary">
                        Primary button
                    </a>
                    </div>
                </div>
                <div class="col d-flex align-items-start">
                    <div class="icon-square text-body-emphasis bg-body-secondary d-inline-flex align-items-center justify-content-center fs-4 flex-shrink-0 me-3">
                    </div>
                    <div>
                    <h3 class="fs-2 text-body-emphasis">Titulo 3</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <a href="#" class="btn btn-primary">
                        Primary button
                    </a>
                    </div>
                </div>
            </div>
        </div>
    );
}