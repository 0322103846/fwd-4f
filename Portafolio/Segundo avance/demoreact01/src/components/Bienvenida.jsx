import React from 'react';

export default function Bienvenida(props){
    return (
        <h1>{props.hola} {props.name} {props.msg} </h1>
    );
}