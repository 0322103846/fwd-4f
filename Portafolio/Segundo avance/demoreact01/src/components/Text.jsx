export default function Text(props){
    return(
        <p style = {styles.colorText} >  {props.p}</p>
    );
}

const styles = {
    colorText: {
        color : "#C269E0",
        textAlign: "center",
        fontSize: 95,
        fontFamily: "Reboto, Helvetica",
    },

}