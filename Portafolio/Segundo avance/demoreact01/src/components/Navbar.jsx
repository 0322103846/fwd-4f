import React from 'react';


export default function Navbar() {
  const estilossolucionadores = {
    '--bs-nav-link-color': 'var(--bs-white)',
    '--bs-nav-pills-link-active-color': 'var(--bs-dark)',
    '--bs-nav-pills-link-active-bg': 'var(--bs-white)'
  };
  return(
    <ul class="nav nav-pills nav-fill gap-2 p-1 small bg-primary rounded-5 shadow-sm" id="pillNav2" role="tablist" style={estilossolucionadores}>
      <li class="nav-item" role="presentation">
        <button class="nav-link active rounded-5" id="home-tab2" data-bs-toggle="tab" type="button" role="tab" aria-selected="true">Home</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link rounded-5" id="profile-tab2" data-bs-toggle="tab" type="button" role="tab" aria-selected="false">Profile</button>
      </li>
      <li class="nav-item" role="presentation">
        <button class="nav-link rounded-5" id="contact-tab2" data-bs-toggle="tab" type="button" role="tab" aria-selected="false">Contact</button>
      </li>
  </ul>
  );
 }
