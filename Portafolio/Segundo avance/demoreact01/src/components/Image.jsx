
import image from '../components/assets/images/image.jpeg';

export default function Image(props){
    return(
        <img style={styles.reSize} src={image} />
    );
}

const styles = {
    reSize: {
        width: 320,
        Height: 440,
        paddingTop: 58,
    },
};