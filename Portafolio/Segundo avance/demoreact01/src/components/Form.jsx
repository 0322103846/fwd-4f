import { useState } from "react";

export default function Form(){ 

    const [firstName, setFirstName] = useState ('');
    const [lastName, setLastName] = useState ('');
    const fullName = firstName + ' ' +lastName

    function handleFirstNameChange(e){
        setFirstName(e.target.value);
    }
    function handleLastNameChange(e){
        setLastName(e.target.value);
    }
    return(
        <>

            <form>
                <h1 class="h3 mb-3 fw-normal">Comenzar</h1>

                <div class="form-floating">
                    <input class="form-control form-control-sm" type="text" value={firstName} onChange={handleFirstNameChange} placeholder="Firts Name" aria-label="First Name"></input> 
                    <label for="floatingInput">Usuario</label>
                </div>
                <div class="form-floating">
                    <input class="form-control form-control-sm" type="text" value={lastName} onChange={handleLastNameChange} placeholder="Last Name" aria-label="Last Name"></input> 
                    <label for="floatingPassword">Contraseña</label>
                </div>
                <button class="btn btn-primary w-100 py-2" type="submit"> Hola, Bienvenidx <strong>{fullName}</strong> , da click para iniciar sesion </button>
            </form>


        
        </>



    );
}