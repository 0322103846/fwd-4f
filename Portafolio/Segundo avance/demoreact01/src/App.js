
import './App.css';
// import image from './image.jpeg';
// const PlaceholderImage = require('./assets/images/image.jpeg')


// import Cabecera from './components/Cabecera';
// import Bienvenida from './components/Bienvenida';
// import Text from './components/Text';
import Image from './components/Image';
// import Table3x3 from './components/Table3x3';
import Navbar from './components/Navbar';
import Content from './components/Content';
import Footer from './components/Footer';
import Form from './components/Form';






function App() {
  return (
    <div className="App">
      {/* <Bienvenida name = "Kariplll" hola = "hola" />
      <Bienvenida msg = "Como estas??" />
      <Text p = "Holaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa etc etc etc etc" /> */}
      {/* <img src= {image} alt="image" /> */}
      {/* <Image />
      <Table3x3 /> */}
      <Navbar />
      <Image />
      <Content />
      <Form />
      <Footer />
      

    </div>
  );
}

export default App;
