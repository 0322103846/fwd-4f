// console.log("mensaje: hola mundo")

// let message = "hola mundo";
// console.log(message);

// var num = 456;
// var flo = 87.32 

// console.log(num)
// console.log(flo)
// console.log(flo + num)

// const pi = 3.1416

// console.log(flo + pi)


// var name = "John"
// var admin = name
// console.log(admin)


// var PTierra = "PTierra"
// var User = "User"

// console.log ("El nombre de la variable para nuestro planeta es: " + PTierra)
// console.log ("El nombre de la variable del usuario: " + User)


// var array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

// for (let i of array) {
//     console.log(i)   
// }


// const string = "Hola";
// for (ch of string){
//     console.log(ch)
// }

// const names = ['bob', 'alejandro', 'zandra', 'anna', 'bob'];

// const uniqueNames = new Set(names);

// for (let name of uniqueNames){
//     console.log(name)
// }


// const obj = {
//     name : "Foo Bar",
//     age: 23, 
//     city: "TJ"
// };


// for (let key of Object.keys(obj)) {
//     console.log(key, ":", obj[key] );
// }


// console.log("-----------------------------------------------")


// for ( let key in obj ) {
//     console.log(key, ":", obj[key] );
// }


// var i = 1000

// while (i< 1000) {
//     console.log(i)
//     i+=5
// }


// do {
//     console.log(i)
//     i+=5
// }while(i<1000)


// var animal = "Kitty"

// if (animal === "Kitty") {
//     console.log("yes, it is a pretty kitty")
// } else {
//     console.log("no, it is not a pretty kitty")
// }



// var cat = ( animal === "Kitty") ? "yes, it is a pretty kitty" : "no, it is not a pretty kitty"
// console.log(cat)


// var classroom = 1 

// switch(classroom) {
//     case 1:
//         console.log("classroom 1")
//         break;
//     case 2 :
//         console.log("classroom 2")
//         break;
//     default: 
//         console.log("Default")
//         break;
// }

// var base = 39
// var altura = 56

// function area(base, altura) {
//     return (base * altura)/2
// }
// console.log("El area del Triangulo: " + area(base, altura))

// var l = 89
// var w = 23
// var h = 12

// function prism(l) {
//     return function(w) {
//         return function(h){
//             return (l * w * h)
//         }
//     }
// }

// console.log(" El volumen del prisma es : " + prism(l)(w)(h))



// (function() {
//     console.log("I am the annonymus function ")
// }());

// const f = (function(msg) {
//     console.log("I am the annonymus function "  + msg)
//     return msg
// }("Foo"));

// console.log(f)

// (() => console.log("Hellooo !! "))();

// const fx = function sum(x, y) {
//     return x + y
// }

// const fy = function(g , h) {
//     return g + h
// }

// console.log("fx: " + fx(45, 221) + " fy: " + fy(65,89))


// var namedSum = function sum (a, b) {
//     return a + b;
// }

// var anonSum = function (a, b) {
//     return a + b;
// }

// namedSum(1, 3)
// anonSum(1, 3)

// var sumTwoNumbers = function sum (a, b) {
//     return a + b;
// }

// sum(1, 3)

// var say = function (times) {
//     if (times > 0) {
//         console.log('Holaap!!')

//         say (times - 1)
//     }
// }

// var sayHelloTimes = say;

// sayHelloTimes(1000);

// // v5
// function logSomeThings() {
//     for (var i = 0; i < arguments.length; ++i) {
//         console.log(arguments [i]);
//     }
// }

// logSomeThings('hello', 'world');


// //v6

// function personLogsSomeThings(person, ...msg) {
//     msg.forEach(arg => {
//         console.log(person, 'says', arg);
//     }); 
// }
// personLogsSomeThings('Kari', 'Holap', '¿como estas?', 'que su color fav es el blanco', 'Holap de nuevo', 'que julio es un racista', 'Bay')

const logarg = (...args) => console.log(args)
const list = [1, 2, 3, 4]

logarg("a", "b", "c", ...list)


function personSay(person, ...msg) {
    msg.forEach(arg => {
        console.log(person + " say : " + arg)
    })
}

personSay("Foo", "Hello", "World", "JS", "REACT", "REACT NATIVE", "ETC") 


