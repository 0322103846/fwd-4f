// var url = 'http://api.stackexchange.com/2.2/questions?site=stackoverflow&tagged=javascript';

// const responseData = fetch(url).then(response => response.json());
// responseData.then(({items, has_more, quota_max, quota_remaining}) => {
//     for (const {title, score, owner, link, answer_count} of items) {
//         console.log( "Question Title: ----------- " + title)
//     }
// });


var url = "https://jsonplaceholder.typicode.com/users"

// fetch(url).then(response => response.json())
//     .then(response => {
//         for (var key in response) {
//             console.log(
//                 "Username: " + response[key].username
//                 + "     email: " + response[key].email
//                 + "     company: " + response[key].company.name 
//                 + "     city: " + response[key].address.city 
//                 + "     zipcode: " + response[key].address.zipcode)
//         }
//     })



// var url = "https://jsonplaceholder.typicode.com/albums"

// fetch(url).then(response => response.json())
//     .then(response => {
//         for (var key in response) {
//             console.log(
//                 "UserId: " + response[key].userId
//                 + "     Id: " + response[key].id
//                 + "     Title: " + response[key].title)
//         }
//     })


// fetch(url, {
//     method: "POST",
//     headers : {
//         "Content_type" : "application/json"
//     },
//     body: JSON.stringify({
//         userId : 101,
//         id : 101,
//         title : "Foo Bar Title"
//     })
// }).then(response => response.json())
//     .then(response => console.log(response) )
    

fetch(url, {
    method: "POST",
    headers : {
        "Content_type" : "application/json"
    },
    body: JSON.stringify({
        id : 11,
        name : "UserEx",
        username : "Foo Bar Title",
        email : "userexample@user.com",
        address: {
            street: "Leandro Valle",
            suite: "Apeet",
            city: "Tijuana",
            geo: {
                lat: "-123.444",
                lng: "123.123",
            }
        },
        phone: "123456789",
        website: "Userexa.org",
        company: {
            name: "Rogerrs",
            catchPhrase: "qwertyuio-asdfghjk",
            bs: "sdfghjerty",
        },
    })
}).then(response => response.json())
    .then(response => console.log(response))
