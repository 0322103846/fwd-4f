import { Route, Routes, BrowserRouter } from 'react-router-dom';
import MainWrapper from './layauts/MainWrapper';
import AdminRuta from './layauts/AdminRuta';
import PrivateRoute from './layauts/PrivateRoute'
import { Toaster } from 'react-hot-toast';

import Cardd from "./layauts/cart"
import Home from './pages/Home'
import Blog from './pages/Blog';
import About from './pages/About';
import Contact from './pages/Contact';
import Log from './pages/Log';
import Sign from './pages/Sign';
import Services from "./pages/Services";
import Tienda from "./pages/Tienda";
import Logout from './pages/logout';
import Perfil from'./pages/perfil';
import Producto from './pages/productos';
import Direccion from './pages/Direccion';
import Carrito from './pages/carrito';
import Shop2 from './pages/tienda2';
import Campaign from './pages/ApiCrudsPages/campaign/Campaign';
import CampaignForm from './pages/ApiCrudsPages/campaign/CampaignForm';
import Category from './pages/ApiCrudsPages/category/Category';
import CategoryForm from './pages/ApiCrudsPages/category/CategoryForm';
import Customer from './pages/ApiCrudsPages/customer/Customer';
import CustomerForm from './pages/ApiCrudsPages/customer/CustomerForm';
import ItemPedido from './pages/ApiCrudsPages/itempedido/ItemPedido';
import ItemPedidoForm from './pages/ApiCrudsPages/itempedido/ItemPedidoForm';
import Pedido from './pages/ApiCrudsPages/pedido/Pedido';
import PedidoForm from './pages/ApiCrudsPages/pedido/PedidoForm';
import ProductoCrud from './pages/ApiCrudsPages/product/Producto';
import ProductForm from './pages/ApiCrudsPages/product/ProductForm';
import Mipedido from './pages/pedidos';

import AdminPage from './pages/AdminPage';

export default function Rutas(){
    return(
    <BrowserRouter>
        <MainWrapper>
            <Routes>        
                <Route path="/adm" element={<AdminPage />} />


                <Route path="/" element={<Home />} />
                <Route path="/blog" element={<Blog />} />
                <Route path="/nosotros" element={<About />} />
                <Route path="/contacto" element={<Contact />} />
                <Route path="/tienda" element={<Tienda />} />
                <Route path="/tienda/:gen" element={<Shop2 />} />

                <Route path="/servicios" element={<Services />} />
                
                <Route path="/login" element={<Log />} />
                <Route path="/register" element={<Sign />} />
                <Route path="/salir" element={<Logout />} />
                <Route path="/perfil" element={<PrivateRoute><Perfil /></PrivateRoute>} />
                <Route path="producto/:num" element={<Producto />}  />
                <Route path="fomulario/direccion" element={<Direccion />}  />
                <Route path="/carrito" element={<Cardd><PrivateRoute><Carrito /></PrivateRoute></Cardd>} />




                <Route path = "/ReCampaigns" element = {<Campaign />} />
                <Route path = "/ReCampaigns/:id"  element = {<CampaignForm />} />
                <Route path = "/CampaignForm"  element = {<CampaignForm />} />
                <Route path = "/ReCategory"  element = {<Category />} />
                <Route path = "/ReCategory/:id"  element = {<CategoryForm />} />
                <Route path = "/CategoryForm"  element = {<CategoryForm />} />
                <Route path = "/ReCustomer"  element = {<Customer />} />
                <Route path = "/ReCustomer/:id"  element = {<CustomerForm />} />
                <Route path = "/ReItemPedido"  element = {<ItemPedido />} />
                <Route path = "/ReItemPedido/:id"  element = {<ItemPedidoForm />} />
                <Route path = "/RePedido"  element = {<Pedido />} />
                <Route path = "/RePedido/:id"  element = {<PedidoForm />} />
                <Route path = "/ReProducto"  element = {<ProductoCrud />} />
                <Route path = "/ReProducto/:id"  element = {<ProductForm />} />
                <Route path = "/ProductForm"  element = {<ProductForm />}/>
                
                <Route path = "/pedidos"  element = {<Mipedido />}/>






            </Routes>
            <Toaster/>
        </MainWrapper>
    </BrowserRouter>
    )
}