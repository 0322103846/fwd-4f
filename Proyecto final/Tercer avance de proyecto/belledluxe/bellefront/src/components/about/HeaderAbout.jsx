
import ImgAbout from "../../assets/images/ImgAbout.jpg"

export default function HeaderAbout() {
    return (
      <img
        className="h-49 w-full object-cover object-center"
        src={ImgAbout}
        alt="nature image"
        style={{borderBottom : "1px solid white" }}
      />
    );
  }