import React from 'react';

export default function AdminWar(){
    return(
        <div>
            <div className="bg-blue-950 text-white py-12 px-6">
                <div className="max-w-3xl mx-auto">
                    <h2 className="text-3xl font-bold mb-6">Aviso Importante: Área Restringida para Uso Exclusivo del Personal Autorizado de Belle d'Luxe</h2>
                    <p className="text-lg mb-6">Estimado equipo de Belle d'Luxe,</p>
                    <p className="text-lg mb-6">Bienvenido a la sección de administración, un espacio reservado para el personal autorizado de Belle d'Luxe. Este área contiene información confidencial y herramientas críticas para el funcionamiento eficiente de nuestro e-commerce.</p>
                    <p className="text-lg mb-6">Por favor, ten en cuenta las siguientes directrices:</p>
                    <ul className="list-disc pl-6 mb-6">
                    <li className="text-lg mb-2">Acceso Restringido: Este espacio está diseñado exclusivamente para el personal autorizado. Cualquier intento de acceso no autorizado se considera una violación de la seguridad y puede estar sujeto a consecuencias legales.</li>
                    <li className="text-lg mb-2">Confidencialidad: La información contenida en esta área es confidencial y solo debe ser compartida con aquellos que tienen la debida autorización. Asegúrate de proteger tu información de inicio de sesión y de cerrar la sesión después de cada uso.</li>
                    <li className="text-lg mb-2">Responsabilidad: El personal autorizado es responsable de cualquier acción realizada en esta área bajo sus credenciales. Por favor, realiza tus tareas con cuidado y notifica al equipo de administración de cualquier actividad sospechosa de inmediato.</li>
                    <li className="text-lg mb-2">Soporte Técnico: En caso de cualquier problema técnico o pregunta relacionada con la administración, por favor, contacta a nuestro equipo de soporte técnico de inmediato. Estamos aquí para ayudarte.</li>
                    </ul>
                    <p className="text-lg mb-6">Apreciamos tu compromiso con la seguridad y la integridad de Belle d'Luxe. Recuerda que tu acceso a esta área es fundamental para mantener la eficiencia y el éxito de nuestro e-commerce.</p>
                    <p className="text-lg font-semibold mb-2">Gracias por tu dedicación y cooperación.</p>
                    <p className="text-lg font-semibold">Atentamente, Belle d'Luxe Administración.</p>
                </div>
            </div>
        </div>
    );
}