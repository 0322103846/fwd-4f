import React from 'react';
import { NavLink } from 'react-router-dom';

export default function AdminSlidebar() {
    return (
        <div className="min-h-screen flex flex-col w-56 bg-gradient-to-b from-gray-900 to-black rounded-r-3xl overflow-hidden shadow-lg">
            <div className="flex items-center justify-center h-20 bg-gray-900">
                <h1 className="text-3xl text-gray-100 uppercase">BELLE D' LUXE</h1>
            </div>
            <ul className="flex flex-col py-4">
                <li>
                    <span className="block text-lg text-gray-300 font-semibold py-2 px-4 bg-gray-900">Agregar o actualizar:</span>
                </li>
                <li>
                    <NavLink to="/ReProducto" activeClassName="bg-gray-800" className="flex items-center h-12 text-gray-300 hover:text-white px-6">
                        <span className="text-sm font-semibold">Productos</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/ReCampaigns" activeClassName="bg-gray-800" className="flex items-center h-12 text-gray-300 hover:text-white px-6">
                        <span className="text-sm font-semibold">Campañas</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/ReCategory" activeClassName="bg-gray-800" className="flex items-center h-12 text-gray-300 hover:text-white px-6">
                        <span className="text-sm font-semibold">Categorías</span>
                    </NavLink>
                </li>
                <li>
                    <br />
                    <span className="block text-lg text-gray-300 font-semibold py-2 px-4 bg-gray-900 mt-4">Solo actualizar:</span>
                </li>
                <li>
                    <NavLink to="/ReCustomer" activeClassName="bg-gray-800" className="flex items-center h-12 text-gray-300 hover:text-white px-6">
                        <span className="text-sm font-semibold">Clientes</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/RePedido" activeClassName="bg-gray-800" className="flex items-center h-12 text-gray-300 hover:text-white px-6">
                        <span className="text-sm font-semibold">Pedidos</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink to="/ReItemPedido" activeClassName="bg-gray-800" className="flex items-center h-12 text-gray-300 hover:text-white px-6">
                        <span className="text-sm font-semibold">Subpedidos</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink exact to="/" activeClassName="bg-gray-800" className="flex items-center h-12 text-gray-300 hover:text-white px-6">
                        <span className="block text-lg text-gray-300 font-semibold py-2 px-4 bg-gray-900 mt-4">Salir</span>
                    </NavLink>
                </li>
            </ul>
        </div>
    );
}
