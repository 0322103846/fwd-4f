import React from 'react';

const Error = () => {
  return (
    <>
      <div className="flex justify-center items-center" style={{marginTop : "100px"}}>
        <div className="border-2 border-solid border-white">
          <h1 className="text-3xl text-white" style={{ padding: "100px" }}>Esta página no existe</h1>
        </div>
      </div>

      <div className="flex justify-center items-center">
        <a href="/">
          <button style={{ border: "none", borderRadius: "9999px", padding: "20px 40px", color: "white", background: "transparent", cursor: "pointer" }}>
            <span style={{ marginRight: "10px" }}>←</span> Regresar a la página principal.
          </button>
        </a>
      </div>
    </>
  );
};

export default Error;
