import React from "react";

const CardHome = ({ title, description, imageUrl }) => {
  return (
    <div className="max-w-sm rounded overflow-hidden shadow-lg">

      <a href={`/tienda`}>
      <img className="w-full" src={imageUrl} alt={title} />
      </a>
      <div className="px-6 py-4">
        <div className="text-white font-bold text-xl mb-2">{title}</div>
        <p className="text-white text-base">{description}</p>
      </div>
    </div>
  );
};


export default CardHome;
