import React, { useState } from 'react';

export default function ToolHome() {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenu = () => {
    setIsOpen(!isOpen);
  };

  return (
    <nav className="bg-[#11131F] py-4 border-t border-b border-white">
      <div className="container mx-auto lg:flex lg:justify-center lg:items-center">
        
 
        <div className="hidden lg:flex lg:space-x-24">
          <a href={`/nosotros`} className="text-white hover:text-gray-300">Sobre nosotros</a>
          <a href={`/tienda`} className="text-white hover:text-gray-300">Tienda</a>
          <a href={`/servicios`} className="text-white hover:text-gray-300">Servicios</a>
          <a href={`/contacto`} className="text-white hover:text-gray-300">Contacto</a>
          <a href={`/blog`} className="text-white hover:text-gray-300">Blog</a>
        </div>
        
     
        <div className="block lg:hidden">
          <button onClick={toggleMenu} className="text-white hover:text-gray-300 focus:outline-none">
            <svg className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              {isOpen ? (
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M6 18L18 6M6 6l12 12" />
              ) : (
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16m-7 6h7" />
              )}
            </svg>
          </button>
        </div>
      </div>

      {isOpen && (
        <div className="lg:hidden" >
         
          <div className="flex flex-col space-y-2 mt-2" >
            <center>
              <a href={`/nosotros`} className="text-white hover:text-gray-300">Sobre nosotros</a>
            </center>
            <center>
              <a href={`/tienda`} className="text-white hover:text-gray-300">Tienda</a>
            </center>
            <center>
              <a href={`/servicios`} className="text-white hover:text-gray-300">Servicios</a>
            </center>
            <center>
              <a href={`/contacto`} className="text-white hover:text-gray-300">Contacto</a>
            </center>
            <center>
              <a href={`/blog`} className="text-white hover:text-gray-300">Blog</a>
            </center> 
          </div>
          
        </div>
      )}
    </nav>
  );
}
