import React, { useEffect, useState } from "react";
import { getAllItemPedidos } from "../../../api/itempedido_api";
import VistItemPedidio from "./VistItemPedido";

export default function ReItemPedido() {
    const [ItemPedido, setItemPedido] = useState([]);

    useEffect(() => {
        async function loadItemPedidos(){
            const resp = await getAllItemPedidos();
            setItemPedido(resp.data);
        }
        loadItemPedidos();
    }, []);

    return(
        <div className="bg-gray-900 text-white min-h-screen p-8">
            <h1 className="text-3xl font-bold mb-6">Subpedidos</h1>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                {ItemPedido.map(ItemPedido => (
                    <VistItemPedidio key={ItemPedido.id} ItemPedido={ItemPedido} />
                ))}
            </div>
        </div>
    );
}
