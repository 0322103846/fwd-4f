import React, { useState } from 'react';
import { Input, Button } from "@material-tailwind/react";
import { Typography } from "@material-tailwind/react";

export default function FooterApi() {

    return (
    <footer style={{ border: '1px solid white', textAlign: 'center' }}>
      <div style={{ margin: '0 auto', padding: '20px', maxWidth: '750px' }}>
        <Typography style={{ marginTop: '20px', color: 'white' }}>
          &copy; 2024 All RUBIS COLLECTION | @belledeluxe. Tijuana, Baja California.
        </Typography>
        <Typography>
          <a href="/politicas" style={{ color: 'white', textDecoration: 'none' }}>Política de privacidad y cookies | Condiciones de venta</a>
        </Typography>
      </div>
    </footer>
  ); 
}
