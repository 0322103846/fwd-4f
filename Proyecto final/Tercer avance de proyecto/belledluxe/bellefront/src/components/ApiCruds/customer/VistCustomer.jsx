import React from 'react';
import { useNavigate } from 'react-router-dom';

export default function VistCustomer({ Customer }) {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate('../ReCustomer/' + Customer.id);
    };

    return (
        <div
            style={{
                border: '1px solid #eee',
                borderRadius: '8px',
                padding: '20px',
                marginBottom: '20px',
                cursor: 'pointer',
                transition: 'all 0.3s ease',
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)',
                backgroundColor: '#fff',
                color: '#333',
            }}
            onClick={handleClick}
        >
            <h1 style={{ margin: '5px 0', fontSize: '22px', color: '#555' }}>ID: {Customer.id}</h1>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Primer nombre: {Customer.name_first}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Apellido(s): {Customer.last_name_pat} {Customer.last_name_mat}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Número de celular: {Customer.phone}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>CP: {Customer.cp}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Calle: {Customer.streat}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Número de casa: {Customer.number_home}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>User: {Customer.user}</p>
        </div>
    );
}
