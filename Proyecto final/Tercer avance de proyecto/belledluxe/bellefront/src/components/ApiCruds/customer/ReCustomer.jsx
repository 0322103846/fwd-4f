import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import { getAllCustomers } from "../../../api/Customer_api";
import VistCustomer from "./VistCustomer";

export default function ReCustomer() {
    const [Customer, setCustomers] = useState([]);

    useEffect(() => {
        async function loadCustomers(){
            const resp = await getAllCustomers();
            setCustomers(resp.data);
        }
        loadCustomers();
    }, []);

    return (
        <div className="bg-gray-900 text-white min-h-screen p-8">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                {Customer.map(Customer => (
                    <VistCustomer key={Customer.id} Customer={Customer} />
                ))}
            </div>
        </div>
    );
}
