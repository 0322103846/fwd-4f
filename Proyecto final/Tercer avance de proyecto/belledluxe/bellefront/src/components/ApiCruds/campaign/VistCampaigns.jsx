import { useNavigate } from 'react-router-dom';

export default function VisitCampaigns({ campaign }) {
    const navigate = useNavigate();

    const handleClick = () => {
        navigate('../ReCampaigns/' + campaign.id);
        
    };

    return (
        <div
        style={{
            border: '1px solid #eee',
            borderRadius: '8px',
            padding: '20px',
            marginBottom: '20px',
            cursor: 'pointer',
            transition: 'all 0.3s ease',
            boxShadow: '0 2px 4px rgba(0, 0, 0, 0.1)',
            backgroundColor: '#fff',
            color: '#333',
        }}
            onClick={handleClick}
        >
            <h1 style={{ margin: '5px 0', fontSize: '22px', color: '#555' }}>{campaign.id}</h1>
            <h2 style={{ margin: '5px 0', fontSize: '20px', color: '#222' }}>{campaign.name}</h2>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Año: {campaign.year}</p>
            <p style={{ margin: '5px 0', fontSize: '18px', color: '#444' }}>Último Editor: {campaign.lasteditor}</p>
        </div>
    );
}
