import React, { useEffect, useState } from "react";
import { useForm } from 'react-hook-form';
import { getPedido, UpdatePedido, DeletePedido } from "../../../api/Pedido_api";
import { getAllUsers } from "../../../api/Users";
import { useNavigate, useParams } from 'react-router-dom';
import { toast } from 'react-hot-toast';
import NavApi from "../../../components/ApiCruds/NavApi";
import FooterApi from "../../../components/ApiCruds/FooterApi";

export default function PedidoForm() {
    const [users, setUsers] = useState([]);
    const { register, handleSubmit, formState: { errors }, setValue } = useForm();
    const navigate = useNavigate();
    const params = useParams();
    const handleClick = () => {
        navigate('../RePedido/');
    };

    useEffect(() => {
        async function fetchData() {

            const usersResponse = await getAllUsers();
            setUsers(usersResponse.data);

            if (params.id) {
                const { data } = await getPedido(params.id);
                setValue('usuario', data.usuario);
                setValue('codigo_pedido', data.codigo_pedido);
            }
        }
        fetchData();
    }, [params.id, setValue]);

    const onSubmit = handleSubmit(async data => {
        if (params.id) {
            await UpdatePedido(params.id, data);
            toast.success('Pedido actualizado con éxito');
        } else {
            toast.error('Pedido no encontrado, inténtelo de nuevo');
        }
        navigate('../RePedido');
    });

    return (
        <div>
            <NavApi/>
        <div className="container mx-auto mt-10 text-white">
            <form onSubmit={onSubmit} className="max-w-md mx-auto">
                <label htmlFor="usuario" className="block mb-2">Usuario:</label>
                <select id="usuario" {...register("usuario", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white">
                    {users.map(user => (
                        <option key={user.id} value={user.id}>{user.username}</option>
                    ))}
                </select> 
                {errors.usuario && <span className="text-red-500">Este campo es requerido</span>}

                <label htmlFor="codigo_pedido" className="block mb-2">Código del pedido:</label>
                <input type="text" id="codigo_pedido" {...register("codigo_pedido", { required: true })} className="block w-full px-4 py-2 mb-4 border border-gray-600 rounded focus:outline-none bg-gray-800 text-white focus:border-blue-500 focus:ring-blue-500"  placeholder="Código del pedido" maxLength={10}/>
                {errors.codigo_pedido && <span className="text-red-500">Este campo es requerido</span>}


               <button type="submit" className="block w-full px-4 py-2 bg-blue-500 rounded hover:bg-blue-600 focus:outline-none focus:bg-blue-600">Guardar</button>
 
               {params.id &&
                <button onClick={async () => {
                    const accepted = window.confirm("¿Estás seguro de que deseas eliminar el registro de este pedido definitivamente?");
                    if (accepted) {
                        await DeletePedido(params.id);
                        navigate('../RePedido');
                        toast.success('Pedido eliminado');
                    }
                }} className="block w-full px-4 py-2 mt-4 bg-red-500 rounded hover:bg-red-600 focus:outline-none focus:bg-red-600">Eliminar</button>}

<br />
                <hr />
                <br />
                <button onClick={handleClick} className="block w-full px-4 py-2 bg-gray-500 rounded hover:bg-gray-600 focus:outline-none focus:bg-gray-600">Cancelar</button>


           </form>



 
        </div>

        <br />

            <FooterApi />
        </div>
    );
}
