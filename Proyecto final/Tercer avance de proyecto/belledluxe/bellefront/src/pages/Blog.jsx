import NavBlog from '../components/blog/NavBlog';
import TextBlog from '../components/blog/TextBlog';
import Bloggers from '../components/blog/Bloggers'; 
import FooterHome from '../components/FooterHome';

const containerStyle = {
    backgroundColor: "#11131F",
    border: "1px solid white",
    fontFamily: "Times New Roman, serif",
  };
  
  export default function Blog() {
    return (
      <div style={containerStyle}>
        <NavBlog/>
        <TextBlog/>
        <Bloggers/>
        <br/>
        <br />
        <br />
        <br />
        <FooterHome/>

      </div>
    );
  }
  