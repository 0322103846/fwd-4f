import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import axios from '../utils/axios';
import NavAbout from '../components/about/NavAbout';

const Mipedido = () => {
  const [respuesta, setRespuesta] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const accessToken = Cookies.get('access_token');
        const response = await axios.get('pedidoo/', {
          headers: { Authorization: `Bearer ${accessToken}` } 
        });
        setRespuesta(response.data.respuesta);
      } catch (error) {
        setError(error);
      }
    };

    fetchData();
  }, []);

  if (error) {
    return <div className="text-white">Error: {error.message}</div>;
  } else if (!respuesta) {
    return <div className="text-white">Cargando...</div>;
  } else {
    const { pedidos } = respuesta;

    return (<div>
      <NavAbout/>
      <br/>
      <div className="text-white respuesta">
        <h2 className="text-2xl font-bold text-center">Resumen de Pedidos</h2>
        <div className="grid gap-4 grid-cols-1 md:grid-cols-2 lg:grid-cols-3">
          {pedidos.map((pedido, index) => (
            <PedidoComponente key={index} pedido={pedido} />
          ))}
        </div>
      </div></div>
    );
  }
};

const PedidoComponente = ({ pedido }) => {
  const { cadigo, fecha, items, total } = pedido;

  return (
    <div className="bg-gray-700 text-white p-4 rounded shadow pedido">
      <h3 className="text-xl font-bold">Pedido #{cadigo}</h3>
      <p>Fecha: {fecha}</p>
      <p>Total paga: {total}</p>
      <ul>
        {items.map((item, index) => (
          <li key={index} className="border border-white rounded-md p-4 shadow-sm">
            <p>Nombre del artículo: {item.itemnombre}</p>
            <p>Cantidad: {item.cantidad}</p>
            <p>Total del producto: {item.totalproducto}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Mipedido;
