import axios from 'axios'


const CampaignApi = axios.create({
    baseURL:'http://localhost:8000/api/cruds/campaign/',
 
})

export const getAllCampaigns  = () => CampaignApi.get('/');
export const getCampaign = (id) => CampaignApi.get('/' + id + '/');
export const createCampaign = (campaign) => CampaignApi.post('/', campaign);
export const DeleteCampaign = (id) => CampaignApi.delete('/' + id); 
export const UpdateCampaign = (id, campaign) => CampaignApi.put(`/${id}/`, campaign );

