import axios from 'axios'


const CategoryApi = axios.create({
    baseURL:'http://localhost:8000/api/cruds/category/',

})
export const getAllCategories  = () => CategoryApi.get('/');
export const getCategory = (id) => CategoryApi.get('/' + id + '/');
export const createCategory = (Category) => CategoryApi.post('/', Category);
export const DeleteCategory = (id) => CategoryApi.delete('/' + id); 
export const UpdateCategory = (id, Category) => CategoryApi.put(`/${id}/`, Category );

