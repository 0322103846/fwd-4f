import axios from 'axios'
import Cookies from 'js-cookie';

const CustomerApi = axios.create({
    baseURL:'http://localhost:8000/api/cruds/customer/',

})

export const getAllCustomers  = () => CustomerApi.get('/');
export const getCustomer = (id) => CustomerApi.get('/' + id + '/');
export const DeleteCustomer = (id) => CustomerApi.delete('/' + id); 
export const UpdateCustomer= (id, Customer) => CustomerApi.put(`/${id}/`, Customer );
