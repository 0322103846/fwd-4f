from django.urls import path, include
from api import views
from rest_framework import routers


from rest_framework_simplejwt.views import (
    TokenRefreshView,
)
app_name = 'api'


router = routers.DefaultRouter()
router.register(r'campaign', views.CrudCampaign)
router.register(r'category', views.CrudCategory)
router.register(r'customer', views.CrudCustomer)
router.register(r'productos', views.CrudProductos)
router.register(r'itemPedido', views.CrudItemPedido)
router.register(r'pedido', views.CrudPedido)


urlpatterns = [
    #cruds
    path('cruds/', include(router.urls)),

    path('token/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', views.RegistroAPI, name='auth_register'),
    path('register2/', views.APIdireccion.as_view(), name='auth_register2'),
    
    path('', views.getRoutes),
    path('check-admin-status/', views.check_admin_status),
    path('ver_usuarios/', views.GetUsersView.as_view(), name = "getUsers"),
    path('perfil/', views.GetCustomerView.as_view(), name='apiperfil'),
#a
    path('tienda/', views.APIListaProductos.as_view(), name="api_tienda_producto"),
    path('producto/<int:pk>', views.APIProductos.as_view()),

    path('cart/', views.APIcart_detail.as_view(), name='cart-detail'),
    path('cart/add/', views.AddToCartAPIView.as_view(), name='add-to-cart'),
    path('cart/borrar/', views.BorraraCarritoApi.as_view(), name='borrar-to-cart'),

    path('pedido/add', views.pedidoAPI.as_view(),name='pedidoApi'),
    path('pedidoo/', views.viewpedidoAPI.as_view(), name='pedidoApiver'),


    path('tiendah/', views.APIListaProductosH.as_view(), name='tiendah'),
    path('tiendam/', views.APIListaProductosM.as_view(), name='pediscdoApiver')

]





