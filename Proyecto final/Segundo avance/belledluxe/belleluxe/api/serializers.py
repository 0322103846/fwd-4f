from core.models import *
from rest_framework import serializers
from django.contrib.auth import authenticate


class SeriCampaign(serializers.ModelSerializer):
    class Meta:
        model = campaign
        fields = '__all__'

class SeriCategory(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        
class seriCustomer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = '__all__'

class SeriProduct(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
        
class SeriItemPedido(serializers.ModelSerializer):
    class Meta:
        model = ItemPedido
        fields = '__all__'

class SeriPedido(serializers.ModelSerializer):
    class Meta:
        model = Pedido
        fields = '__all__'


class iniciarSecion(serializers.Serializer):
    usuario = serializers.CharField(label='usuario', write_only=True)
    #trim_whitespace
    contra = serializers.CharField(label='Contraseña', style={'input_type':'password'}, trim_whitespace=False, write_only=True)

    def validate(self, attrs):
        username = attrs.get('usuario')
        password = attrs.get('contra')
        if username and password:
            user = authenticate(request = self.context.get('request'), username=username, password=password)
            if not user:
                msg='Usuario o contraseña son incorrectas'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg='Usuario o contraseña no introducidos'
            raise serializers.ValidationError(msg, code='authorization')
        attrs['user'] = user
        return attrs


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


