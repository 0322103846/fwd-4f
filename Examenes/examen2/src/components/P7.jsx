import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

const columns = [
  {
    name : 'PENDING ID',
    selector : (row) => row.id,
    sortable: true,
  },
  {
    name : 'USER IDS',
    selector : (row) => row.userId,
    sortable: true,
  }
];


const P6 = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
      const fetchData = async () => {
        const response = await fetch('http://jsonplaceholder.typicode.com/todos');
        const json = await response.json();
        const fd = json.filter((task) => !task.completed);
        setData(fd);
      };
  
      fetchData();
    }, []);
      
      return (
        <DataTable
          columns={columns}
          data={data}
          pagination
        />
      );
};

export default P6;