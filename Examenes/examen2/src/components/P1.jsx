import React, { useState, useEffect } from 'react';
import DataTable from 'react-data-table-component';

const columns = [
    {
        name : 'PENDING ID',
        selector : (row) => row.id,
        sortable: true,
    },
];


const P1 = () => {
    const [data, setData] = useState([]);

    useEffect(() => {
      const fetchData = async () => {
        const response = await fetch('http://jsonplaceholder.typicode.com/todos');
        const json = await response.json();
        setData(json);
      };
  
      fetchData();
    }, []);
  
    return (
      <DataTable
        columns={columns}
        data={data}
        pagination
      />
    );


};

export default P1;