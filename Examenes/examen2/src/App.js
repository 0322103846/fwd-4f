import './App.css';
import P1 from './components/P1';
import P2 from './components/P2';
import P3 from './components/P3';
import P4 from './components/P4';
import P5 from './components/P5';
import P6 from './components/P6';
import P7 from './components/P7';
import Logo from './components/logo';
import Footer from './components/Footer';


import React, { useState } from 'react';



const Accordion = ({ items }) => {
  return (
    <div className="accordion">
      {items.map((item, index) => (
        <AccordionItem key={index} title={item.title} content={item.content} />
      ))}
    </div>
  );
};

const AccordionItem = ({ title, content }) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleToggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <div className="accordion-item">
      <button className="accordion-title" onClick={handleToggle}>
        {title}
      </button>
      {isOpen && <div className="accordion-content">{content}</div>}
    </div>
  );
};

const items = [
  {
    title: 'Lista de todos los pendientes (solo IDs)',
    content:       <P1/>
  },
  {
    title: 'Lista de todos los pendientes (IDs y Titulos)',
    content:       <P2/>
  },
  {
    title: 'Lista de todos los pendientes sin resolver (IDs y Titulos)',
    content:       <P3/>
  },
  {
    title: 'Lista de los pendientes resueltos (IDs y Titulos)',
    content:       <P4/>
  },
  {
    title: 'Lista de todos los pendientes (IDs y UserIDs)',
    content:       <P5/>
  },
  {
    title: 'Lista de todos los pendientes resueltos (IDs y UserIDs)',
    content:       <P6/>
  },
  {
    title: 'Lista de todos los pendientes sin resolver (IDs y UserIDs)',
    content:       <P7/>
  },
];

function App() {
  return (
    <div className="App">
      <Logo/>
      <Accordion items={items} />
      <Footer/>
    </div>
  );
}

export default App;